"use strict";
app.service("FarbRepoService", function ($log, Farbe,  ice) {
    $log.debug("FarbRepoService()");
    /**
     * Setzt die Farben auf
     */
    let zufallHex = () => Math.floor(Math.random() * 256);
    let zufallFarbe = (i) => new Farbe(`farbe-${i}`, zufallHex(),  zufallHex(), zufallHex());
    this.farben = StorageService.laden();
    if (this.farben === undefined) {
        for (let i = 0; i < 10; i++) {
            this.farben.push(zufallFarbe(i));
        }
    }

    /**
     * Liefert ein Array mit allen Farbe-Objekten.
     */
    this.farben = () => {
        return farben;
    };
    /**
     * Ersetzt die alte durch die neue Farbe im Farben-Array.
     */
    this.ersetzen = (alt, neu) => {
        let index = farben.indexOf(alt);
        if (index >= 0) {
            farben[index] = neu;
        } else {
            farben.unshift(neu);
        }
    };

    this.farbeLoeschen = (farbe) => {
        let index;
        if ((index = farben.indexOf(farbe)) > -1) {
            console.log(index);
            farben.splice(index, 1);
        }
    }
});