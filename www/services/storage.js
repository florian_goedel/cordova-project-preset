"use strict";

app.service("StorageService", function ($log, Farbe) {

    $log.debug("StorageService()");

    const FARBE_KEY = 'farbe';

    this.speichern = (farbe) => {
        localStorage.setItem(FARBE_KEY, JSON.stringify(FarbRepoService.farben()));
    };

    this.laden = () => {
        let f = JSON.parse(localStorage.getItem(FARBE_KEY));
        if (f === null) {
            f = [];
        }
        for (let i = 0; i < f.length; i++) {
            f.push(new Farbe(f.name, f.rot, f.gruen, f.blau, new Date(f.date)));
        }
        return f;
    };

});
