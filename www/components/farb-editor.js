"use strict";

app.component("farbEditor", {
    templateUrl: "components/farb-editor.html",
    controller: "FarbEditorController",
    bindings: {}
});


app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state({
        name: "farb-editor",
        params: { farbe: null },
        component: "farbEditor"
    });
});


app.controller("FarbEditorController", function ($log, $state, $stateParams, Farbe, FarbRepoService) {

    $log.debug("FarbEditorController()");

    // Arbeitskopie der übergebenen Farbe
    this.farbe = new Farbe(
        $stateParams.farbe.name,
        $stateParams.farbe.rot,
        $stateParams.farbe.gruen,
        $stateParams.farbe.blau);

    this.speichern = () => {
        $log.debug("FarbEditorController.speichern()", this.farbe);
        FarbRepoService.ersetzen($stateParams.farbe, this.farbe);
        $state.go("farb-liste");
    };

});
