"use strict";

app.component("rgbWert", {
    templateUrl: "components/rgb-wert.html",
    controller: "RgbWertController",
    bindings: {
        label: "@",
        wert: "<",
        wertAenderung: "&"
    }
});


app.controller("RgbWertController", function ($log) {

    $log.debug("RgbWertController()");

    this.geaendert = function() {
        this.wertAenderung({ wert: this.wert });
    };
});
