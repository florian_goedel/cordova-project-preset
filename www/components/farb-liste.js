"use strict";

app.component("farbListe", {
    templateUrl: "components/farb-liste.html",
    controller: "FarbListeController",
    bindings: {}
});


app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.state({
        name: "farb-liste",
        url: "/das-ist-meine-homepage",
        component: "farbListe"
    });

    $urlRouterProvider.otherwise("/das-ist-meine-homepage");
});


app.controller("FarbListeController", function ($log, FarbRepoService, $timeout, StorageService) {

    $log.debug("FarbListeController()");

    this.farben = FarbRepoService.farben();
    StorageService.speichern(this.farben);


    // Hier wird ein Nebeneffekt von $timeout ausgenützt:
    // $timeout bewirkt, dass AngularJS-Ausdrücke neu berechnet werden.
    // Wichtig: Date.now() muss _außerhalb_ des Filters aufgerufen werden.
    // Am kürzesten als "immediately invoked function expression" (IIFE) zu formulieren:
    (function aktualisieren() {
        $timeout(aktualisieren, 1000);
    })();

});
