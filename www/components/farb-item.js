"use strict";

app.component("farbItem", {
    templateUrl: "components/farb-item.html",
    controller: "FarbItemController",
    bindings: {
        farbe: "<"
    }
});


app.controller("FarbItemController", function ($log, FarbRepoService) {

    $log.debug("FarbItemController()");

    this.jetzt = () => {
        return Date.now();
    };

    this.farbeLoeschen = () => {
        FarbRepoService.farbeLoeschen(this.farbe);
    };

});
