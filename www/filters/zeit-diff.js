"use strict";

/**
 * Formatiert die Zeitdifferenz zwischen dem Zeitpunkt
 * und dem Bezugszeitpunkt in lesefreundlicher Art.
 */
app.filter("zeitDiff", function (dateFilter) {

    function zeitDiff(zeitpunkt, bezugsZeitpunkt) {
        let diff = zeitpunkt - bezugsZeitpunkt,
            richtung = diff > 0 ? 'in' : 'vor';

        diff = Math.abs(diff);

        // date-Format abhängig von der Größe der Zeitdifferenz wählen
        if (diff < 60 * 1000) {
            // Weniger als eine Minute
            var format = "s 's'";
        } else if (diff < 3600 * 1000) {
            // Weniger als eine Stunde
            var format = "m 'min' s 's'";
        } else if (diff < 6 * 3600 * 1000) {
            // Weniger als 6 Stunden
            var format = "H 'h' m 'min'";
        } else {
            // Nur den letzten Tag im Format berücksichtigen,
            // die Tage werden gesondert berücksichtigt
            var format = "H 'h'";
            diff = diff % (24 * 3600 * 1000);
        }

        // Tage (wenn vorhanden) formatieren
        let tage = Math.floor(diff / (24 * 3600 * 1000));
        tage = tage ? `${tage}T ` : '';

        return `${richtung} ${tage}${dateFilter(new Date(diff), format, 'UTC')}`;
    }

    return zeitDiff;
});
